﻿using Asfw.Network;

namespace server
{
    internal static partial class Network
    {
        internal enum ClientPackets
        {
            FakeLogin,
            Message,
            Count
        }

        internal enum ServerPackets
        {
            KeyPair,
            RelayMessage,
            Count
        }
    }
}
