﻿using Asfw.IO.Encryption;
using System;

namespace client
{
    static class Program
    {

        internal static KeyPair EKeyPair = new KeyPair();

        internal static void Main(string[] args)
        {
            Network.InitNetwork();

            Console.WriteLine("Client ready to connect!");

            var running = true;
            while (running)
            {
                var command = Console.ReadLine().Trim();

                switch (command.ToLower())
                {
                    case "/connect":
                        Network.Connect("127.0.0.1", 4000);
                        break;

                    case "/login":
                        Network.SendFakeLogin("My UserName Here", "My Password Here");
                        break;

                    case "/exit":
                        running = false;
                        break;

                    default:
                        if (Network.IsConnected() && command.Trim().Length > 0)
                            Network.SendMessage(command);
                        break;
                }
            }

            Network.DestroyNetwork();
            Environment.Exit(0);
        }
    }
}
