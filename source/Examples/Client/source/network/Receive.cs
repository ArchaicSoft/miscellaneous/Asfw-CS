﻿using System;
using Asfw;

namespace client
{
    internal static partial class Network
    {
        internal static void PacketRouter()
        {
            // Any time new packets are made that are expected to be receieved
            // which in this case our client receieves serverpackets. Add it to here
            // and route it to the function handler via AddressOf. Do not include count
            // as the count name in the enumerators arent actually created in the network
            // as pointers.
            Socket.PacketId[(int)ServerPackets.KeyPair] = Handle_KeyPair;
            Socket.PacketId[(int)ServerPackets.RelayMessage] = Handle_RelayMessage;
        }

        internal static void Handle_KeyPair(ref byte[] data)
        {
            var buffer = new ByteStream(data);
            Program.EKeyPair.ImportKeyString(buffer.ReadString());
            buffer.Dispose();
        }

        internal static void Handle_RelayMessage(ref byte[] data)
        {
            // Passing an array in the new parameters starts the buffer with 
            // already existing data expected to be read.
            var buffer = new ByteStream(data);

            // Unlike the send functions, we dont read for the header
            // that we always have to write first. The network object
            // already strips that off because it needed to know the 
            // header already to tell PacketID which pointer to invoke.
            // NOTE: Our packetrouter is assigning functions to the array
            // values in PacketID, that is why if a header is invoked it knows
            // which function to call.
            var msg = buffer.ReadString();

            // Cleanup
            buffer.Dispose();

            // NOTE: The network handles async but .net treats
            // it as multithreading so accessing form objects directly
            // is not recommended and will probably fail.
            Console.WriteLine(msg);
        }
    }
}