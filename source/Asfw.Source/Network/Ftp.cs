﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;

namespace Asfw.Network
{
    public sealed class Ftp : IDisposable
    {
        private readonly NetworkCredential _credential;
        public int BufferSize = 8192;

        public Ftp(NetworkCredential credentials)
        {
            _credential = credentials;
        }

        public void Dispose()
        {
            /* Nothing to Dispose */
        }

        public void DeleteFile(string url)
        {
            var request = (FtpWebRequest) WebRequest.Create(url);
            request.Credentials = _credential;
            request.KeepAlive = true;
            request.UseBinary = true;
            request.UsePassive = true;
            request.Method = "DELE";
            request.GetResponse().Close();
        }

        public void DownloadFile(string url, string localFile)
        {
            var request = (FtpWebRequest) WebRequest.Create(url);
            request.Credentials = _credential;
            request.KeepAlive = true;
            request.UseBinary = true;
            request.UsePassive = true;
            request.Method = "RETR";

            using (var response = (FtpWebResponse) request.GetResponse())
            using (var rs = response.GetResponseStream())
            using (var fs = new FileStream(localFile, FileMode.Create))
            {
                if (rs is null)
                    throw new EndOfStreamException("Stream was empty.");

                var buffer = new byte[BufferSize];
                var len = rs.Read(buffer, 0, BufferSize);
                while (len > 0)
                {
                    fs.Write(buffer, 0, len);
                    len = rs.Read(buffer, 0, BufferSize);
                }
            }
        }

        public string GetDateTimestamp(string url)
        {
            var request = (FtpWebRequest) WebRequest.Create(url);
            request.Credentials = _credential;
            request.KeepAlive = true;
            request.UseBinary = true;
            request.UsePassive = true;
            request.Method = "MDTM";

            using (var response = (FtpWebResponse) request.GetResponse())
            using (var rs = response.GetResponseStream())
            {
                if (rs is null) throw new EndOfStreamException("Stream was empty.");
                using (var sr = new StreamReader(rs)) return sr.ReadToEnd();
            }
        }

        public long GetFileSize(string url)
        {
            var request = (FtpWebRequest) WebRequest.Create(url);
            request.Credentials = _credential;
            request.KeepAlive = true;
            request.UseBinary = true;
            request.UsePassive = true;
            request.Method = "SIZE";

            using (var response = (FtpWebResponse) request.GetResponse())
                return response.ContentLength;
        }

        public string[] ListDirectory(string url)
        {
            var request = (FtpWebRequest) WebRequest.Create(url);
            request.Credentials = _credential;
            request.KeepAlive = true;
            request.UseBinary = true;
            request.UsePassive = true;
            request.Method = "NLST";

            using (var response = (FtpWebResponse) request.GetResponse())
            using (var rs = response.GetResponseStream())
            {
                if (rs is null) throw new EndOfStreamException("Stream was empty.");

                using (var sr = new StreamReader(rs))
                {
                    var list = new List<string>();
                    while (sr.Peek() != -1)
                        list.Add(sr.ReadLine());
                    return list.ToArray();
                }
            }
        }

        public string[] ListDirectoryDetails(string url)
        {
            var request = (FtpWebRequest) WebRequest.Create(url);
            request.Credentials = _credential;
            request.KeepAlive = true;
            request.UseBinary = true;
            request.UsePassive = true;
            request.Method = "LIST";

            using (var response = (FtpWebResponse) request.GetResponse())
            using (var rs = response.GetResponseStream())
            {
                if (rs is null) throw new EndOfStreamException("Stream was empty.");

                using (var sr = new StreamReader(rs))
                {
                    var list = new List<string>();
                    while (sr.Peek() != -1)
                        list.Add(sr.ReadLine());
                    return list.ToArray();
                }
            }
        }

        public void MakeDirectory(string url)
        {
            var request = (FtpWebRequest) WebRequest.Create(url);
            request.Credentials = _credential;
            request.KeepAlive = true;
            request.UseBinary = true;
            request.UsePassive = true;
            request.Method = "MKD";
            request.GetResponse().Close();
        }

        public void Rename(string url, string name)
        {
            var request = (FtpWebRequest) WebRequest.Create(url);
            request.Credentials = _credential;
            request.KeepAlive = true;
            request.UseBinary = true;
            request.UsePassive = true;
            request.Method = "RENAME";
            request.RenameTo = name;
            request.GetResponse().Close();
        }

        public void UploadFile(string url, string localFile)
        {
            var request = (FtpWebRequest) WebRequest.Create(url);
            request.Credentials = _credential;
            request.KeepAlive = true;
            request.UseBinary = true;
            request.UsePassive = true;
            request.Method = "STOR";

            using (var rs = request.GetRequestStream())
            using (var fs = new FileStream(localFile, FileMode.Create))
            {
                var buffer = new byte[BufferSize];
                for (var count = fs.Read(buffer, 0, BufferSize);
                    (uint) count > 0U;
                    count = fs.Read(buffer, 0, BufferSize))
                    rs.Write(buffer, 0, count);
            }
        }
    }
}