﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Asfw.IO.Encryption
{
    /// <summary>
    /// Simple encryption methods performed over a static password.
    /// </summary>
    public static class Generic
    {
        public static byte[] EncryptBytes(byte[] value, string password,
            int iterations)
        {
            var len = value.Length;
            var salt = new byte[32];
            var rgbIv = new byte[32];
            byte[] buffer;

            using (var csp = new RNGCryptoServiceProvider())
            {
                csp.GetBytes(salt);
                csp.GetBytes(rgbIv);
            }

            using (var bytes = new Rfc2898DeriveBytes(password, salt, iterations))
            {
                var rm = new RijndaelManaged
                {
                    BlockSize = 128,
                    Mode = CipherMode.CBC,
                    Padding = PaddingMode.PKCS7
                };

                using (var es = rm.CreateEncryptor(bytes.GetBytes(32), rgbIv))
                using (var ms = new MemoryStream())
                using (var cs = new CryptoStream(ms, es, CryptoStreamMode.Write))
                {
                    cs.Write(value, 0, len);
                    cs.FlushFinalBlock();
                    buffer = ms.ToArray();
                }

                len = buffer.Length;
                value = new byte[64 + len];
                Buffer.BlockCopy(salt, 0, value, 0, 32);
                Buffer.BlockCopy(rgbIv, 0, value, 32, 32);
                Buffer.BlockCopy(buffer, 0, value, 64, len);

                return value;
            }
        }

        public static async Task<byte[]> EncryptBytesAsync(byte[] value,
            string password, int iterations)
        {
            var len = value.Length;
            var salt = new byte[32];
            var rgbIv = new byte[32];
            byte[] buffer;

            using (var csp = new RNGCryptoServiceProvider())
            {
                csp.GetBytes(salt);
                csp.GetBytes(rgbIv);
            }

            using (var bytes = new Rfc2898DeriveBytes(password, salt, iterations))
            {
                var rm = new RijndaelManaged
                {
                    BlockSize = 128,
                    Mode = CipherMode.CBC,
                    Padding = PaddingMode.PKCS7
                };

                using (var es = rm.CreateEncryptor(bytes.GetBytes(32), rgbIv))
                using (var ms = new MemoryStream())
                using (var cs = new CryptoStream(ms, es, CryptoStreamMode.Write))
                {
                    await cs.WriteAsync(value, 0, len);
                    cs.FlushFinalBlock();
                    buffer = ms.ToArray();
                }

                len = buffer.Length;
                value = new byte[64 + len];
                Buffer.BlockCopy(salt, 0, value, 0, 32);
                Buffer.BlockCopy(rgbIv, 0, value, 32, 32);
                Buffer.BlockCopy(buffer, 0, value, 64, len);

                return value;
            }
        }

        public static string EncryptString(string value, string password,
            int iterations)
        {
            var buffer = EncryptBytes(Encoding.UTF8.GetBytes(value), password,
                iterations);
            return Convert.ToBase64String(buffer);
        }

        public static async Task<string> EncryptStringAsync(string value,
            string password, int iterations)
        {
            var buffer = await EncryptBytesAsync(Encoding.UTF8.GetBytes(value),
                password, iterations);
            return Convert.ToBase64String(buffer);
        }

        public static byte[] DecryptBytes(byte[] value, string password,
            int iterations)
        {
            var len = value.Length - 64;
            var salt = new byte[32];
            var rgbIv = new byte[32];
            var buffer = new byte[len];

            Buffer.BlockCopy(value, 0, salt, 0, 32);
            Buffer.BlockCopy(value, 32, rgbIv, 0, 32);
            Buffer.BlockCopy(value, 64, buffer, 0, len);

            var rm = new RijndaelManaged
            {
                BlockSize = 128,
                Mode = CipherMode.CBC,
                Padding = PaddingMode.PKCS7
            };

            using (var bytes = new Rfc2898DeriveBytes(password, salt, iterations))
            using (var ds = rm.CreateDecryptor(bytes.GetBytes(32), rgbIv))
            using (var ms = new MemoryStream())
            using (var cs = new CryptoStream(ms, ds, CryptoStreamMode.Write))
            {
                cs.Write(buffer, 0, len);
                cs.FlushFinalBlock();
                return ms.ToArray();
            }
        }

        public static async Task<byte[]> DecryptBytesAsync(byte[] value,
            string password, int iterations)
        {
            var len = value.Length - 64;
            var salt = new byte[32];
            var rgbIv = new byte[32];
            var buffer = new byte[len];

            Buffer.BlockCopy(value, 0, salt, 0, 32);
            Buffer.BlockCopy(value, 32, rgbIv, 0, 32);
            Buffer.BlockCopy(value, 64, buffer, 0, len);

            var rm = new RijndaelManaged
            {
                BlockSize = 128,
                Mode = CipherMode.CBC,
                Padding = PaddingMode.PKCS7
            };

            using (var bytes = new Rfc2898DeriveBytes(password, salt, iterations))
            using (var ds = rm.CreateDecryptor(bytes.GetBytes(32), rgbIv))
            using (var ms = new MemoryStream())
            using (var cs = new CryptoStream(ms, ds, CryptoStreamMode.Write))
            {
                await cs.WriteAsync(buffer, 0, len);
                cs.FlushFinalBlock();
                return ms.ToArray();
            }
        }

        public static string DecryptString(string value, string password,
            int iterations)
        {
            var buffer = DecryptBytes(Convert.FromBase64String(value),
                password, iterations);
            return Encoding.UTF8.GetString(buffer);
        }

        public static async Task<string> DecryptStringAsync(string value,
            string password, int iterations)
        {
            var buffer = await DecryptBytesAsync(Convert.FromBase64String(value),
                password, iterations);
            return Encoding.UTF8.GetString(buffer);
        }
    }

    /// <summary>
    /// Advanced encryption methods performed over a generated Async Keypair Code.
    /// </summary>
    public sealed class KeyPair : IDisposable
    {
        /// <summary>
        /// Honestly have no idea what these are for, but they are used in key
        /// generation so if anyone knows their purpose they'll still have the
        /// ability to use them.
        /// </summary>
        public enum KeyType
        {
            Signature = 1,
            Exchange = 2,
        }

        private RSACryptoServiceProvider _rsa;

        public void Dispose()
        {
            _rsa.Dispose();
            _rsa = null;
        }

        /// <summary>
        /// Returns if only a Public/Encryption key was loaded.
        /// </summary>
        public bool PublicOnly
        {
            get
            {
                if (_rsa is null) throw new CryptographicException("Key(s) not found!");
                return _rsa.PublicOnly;
            }
        }

        /// <summary>
        /// Generates a new Public/Encryption and Private/Decryption Keypair.
        /// </summary>
        public void GenerateKeys()
        {
            _rsa = new RSACryptoServiceProvider(2048);
        }

        /// <summary>
        /// Returns the string data of the Key Code(s). This functions
        /// primary use is for retrieving Key(s) so they can be passed
        /// to other KeyPair objects without access to a file. (EX: over a network)
        /// </summary>
        public string ExportKeyString(bool exportPrivate = false)
        {
            return _rsa.ToXmlString(exportPrivate);
        }

        /// <summary>
        /// Saves the string data of the Key Code(s) to a file for reuse.
        /// </summary>
        public void ExportKey(string file, bool exportPrivate = true)
        {
            var stream = new StreamWriter(file, false);
            stream.Write(_rsa.ToXmlString(exportPrivate));
            stream.Close();
        }

        /// <summary>
        /// Loads the string data of the Key Code(s) for use.
        /// </summary>
        public void ImportKeyString(string key)
        {
            _rsa = new RSACryptoServiceProvider();
            _rsa.FromXmlString(key);
        }

        /// <summary>
        /// Loads the string data of the Key Code(s) from a file for use.
        /// </summary>
        public void ImportKey(string file)
        {
            var stream = new StreamReader(file);
            _rsa = new RSACryptoServiceProvider();
            _rsa.FromXmlString(stream.ReadToEnd());
            stream.Close();
        }

        public byte[] EncryptBytes(byte[] value)
        {
            if (_rsa is null) throw new CryptographicException("Key not set.");

            var rm = new RijndaelManaged
            {
                KeySize = 256,
                BlockSize = 128,
                Mode = CipherMode.CBC
            };
            

            using (var ms = new MemoryStream())
            {
                ms.Write(_rsa.Encrypt(rm.Key, false), 0, 256);
                ms.Write(rm.IV, 0, 16);

                using (var es = rm.CreateEncryptor())
                using (var cs = new CryptoStream(ms, es, CryptoStreamMode.Write))
                {
                    cs.Write(value, 0, value.Length);
                    cs.FlushFinalBlock();
                }

                return ms.ToArray();
            }
        }

        public async Task<byte[]> EncryptBytesAsync(byte[] value)
        {
            if (_rsa is null) throw new CryptographicException("Key not set.");

            var rm = new RijndaelManaged
            {
                KeySize = 256,
                BlockSize = 128,
                Mode = CipherMode.CBC
            };

            using (var ms = new MemoryStream())
            {
                await ms.WriteAsync(_rsa.Encrypt(rm.Key, false), 0, 256);
                await ms.WriteAsync(rm.IV, 0, 16);

                using (var es = rm.CreateEncryptor())
                using (var cs = new CryptoStream(ms, es, CryptoStreamMode.Write))
                {
                    await cs.WriteAsync(value, 0, value.Length);
                    cs.FlushFinalBlock();
                }

                return ms.ToArray();
            }
        }

        public byte[] EncryptBytes(byte[] value, int offset, int size)
        {
            if (_rsa is null) throw new CryptographicException("Key not set.");

            var rm = new RijndaelManaged
            {
                KeySize = 256,
                BlockSize = 128,
                Mode = CipherMode.CBC
            };

            using (var es = rm.CreateEncryptor())
            using (var ms = new MemoryStream())
            {
                ms.Write(_rsa.Encrypt(rm.Key, false), 0, 256);
                ms.Write(rm.IV, 0, 16);

                using (var cs = new CryptoStream(ms, es, CryptoStreamMode.Write))
                {
                    cs.Write(value, offset, size);
                    cs.FlushFinalBlock();
                }

                return ms.ToArray();
            }
        }

        public async Task<byte[]> EncryptBytesAsync(byte[] value, int offset,
            int size)
        {
            if (_rsa is null) throw new CryptographicException("Key not set.");

            var rm = new RijndaelManaged
            {
                KeySize = 256,
                BlockSize = 128,
                Mode = CipherMode.CBC
            };

            using (var es = rm.CreateEncryptor())
            using (var ms = new MemoryStream())
            {
                await ms.WriteAsync(_rsa.Encrypt(rm.Key, false), 0, 256);
                await ms.WriteAsync(rm.IV, 0, 16);

                using (var cs = new CryptoStream(ms, es, CryptoStreamMode.Write))
                {
                    await cs.WriteAsync(value, offset, size);
                    cs.FlushFinalBlock();
                }

                return ms.ToArray();
            }
        }

        public string EncryptString(string value)
        {
            if (_rsa is null) throw new CryptographicException("Key not set.");
            return Convert.ToBase64String(EncryptBytes(
                Encoding.UTF8.GetBytes(value)));
        }

        public async Task<string> EncryptStringAsync(string value)
        {
            if (_rsa is null) throw new CryptographicException("Key not set.");
            return Convert.ToBase64String(
                await EncryptBytesAsync(Encoding.UTF8.GetBytes(value)));
        }

        public void EncryptFile(string srcFile, string dstFile)
        {
            if (_rsa is null) throw new CryptographicException("Key not set.");
            File.WriteAllBytes(dstFile, EncryptBytes(File.ReadAllBytes(srcFile)));
        }

        public async Task EncryptFileAsync(string srcFile, string dstFile)
        {
            if (_rsa is null) throw new CryptographicException("Key not set.");
            File.WriteAllBytes(dstFile,
                await EncryptBytesAsync(File.ReadAllBytes(srcFile)));
        }

        public byte[] DecryptBytes(byte[] value)
        {
            if (_rsa is null) throw new CryptographicException("Key not set.");
            if (_rsa.PublicOnly) return null;
            if (value.Length < 272) return null;

            var rm = new RijndaelManaged
            {
                KeySize = 256,
                BlockSize = 128,
                Mode = CipherMode.CBC
            };

            var rgb = new byte[256];
            var iv = new byte[16];
            var len = value.Length - 272;
            var buffer = new byte[len];
            Buffer.BlockCopy(value, 0, rgb, 0, 256);
            Buffer.BlockCopy(value, 256, iv, 0, 16);
            Buffer.BlockCopy(value, 272, buffer, 0, len);

            using (var ds = rm.CreateDecryptor(_rsa.Decrypt(rgb, false), iv))
            using (var ms = new MemoryStream())
            using (var cs = new CryptoStream(ms, ds, CryptoStreamMode.Write))
            {
                cs.Write(buffer, 0, len);
                cs.FlushFinalBlock();
                return ms.ToArray();
            }
        }

        public async Task<byte[]> DecryptBytesAsync(byte[] value)
        {
            if (_rsa is null) throw new CryptographicException("Key not set.");
            if (_rsa.PublicOnly) return null;
            if (value.Length < 272) return null;

            var rm = new RijndaelManaged
            {
                KeySize = 256,
                BlockSize = 128,
                Mode = CipherMode.CBC
            };

            var rgb = new byte[256];
            var iv = new byte[16];
            var len = value.Length - 272;
            var buffer = new byte[len];
            Buffer.BlockCopy(value, 0, rgb, 0, 256);
            Buffer.BlockCopy(value, 256, iv, 0, 16);
            Buffer.BlockCopy(value, 272, buffer, 0, len);

            using (var ds = rm.CreateDecryptor(_rsa.Decrypt(rgb, false), iv))
            using (var ms = new MemoryStream())
            using (var cs = new CryptoStream(ms, ds, CryptoStreamMode.Write))
            {
                await cs.WriteAsync(buffer, 0, len);
                cs.FlushFinalBlock();
                return ms.ToArray();
            }
        }

        public byte[] DecryptBytes(byte[] value, int offset, int size)
        {
            if (_rsa is null) throw new CryptographicException("Key not set.");
            if (_rsa.PublicOnly) return null;
            if (value.Length < 272) return null;
            if (value.Length < offset + size) return null;

            var rm = new RijndaelManaged
            {
                KeySize = 256,
                BlockSize = 128,
                Mode = CipherMode.CBC
            };

            var rgb = new byte[256];
            var iv = new byte[16];
            var len = size - 272;
            var buffer = new byte[len];
            Buffer.BlockCopy(value, offset, rgb, 0, 256);
            Buffer.BlockCopy(value, offset + 256, iv, 0, 16);
            Buffer.BlockCopy(value, offset + 272, buffer, 0, len);

            using (var ds = rm.CreateDecryptor(_rsa.Decrypt(rgb, false), iv))
            using (var ms = new MemoryStream())
            using (var cs = new CryptoStream(ms, ds, CryptoStreamMode.Write))
            {
                cs.Write(buffer, 0, len);
                cs.FlushFinalBlock();
                return ms.ToArray();
            }
        }

        public async Task<byte[]> DecryptBytesAsync(byte[] value, int offset,
            int size)
        {
            if (_rsa is null) throw new CryptographicException("Key not set.");
            if (_rsa.PublicOnly) return null;
            if (value.Length < 272) return null;
            if (value.Length < offset + size) return null;

            var rm = new RijndaelManaged
            {
                KeySize = 256,
                BlockSize = 128,
                Mode = CipherMode.CBC
            };

            var rgb = new byte[256];
            var iv = new byte[16];
            var len = size - 272;
            var buffer = new byte[len];
            Buffer.BlockCopy(value, offset, rgb, 0, 256);
            Buffer.BlockCopy(value, offset + 256, iv, 0, 16);
            Buffer.BlockCopy(value, offset + 272, buffer, 0, len);

            using (var ds = rm.CreateDecryptor(_rsa.Decrypt(rgb, false), iv))
            using (var ms = new MemoryStream())
            using (var cs = new CryptoStream(ms, ds, CryptoStreamMode.Write))
            {
                await cs.WriteAsync(buffer, 0, len);
                cs.FlushFinalBlock();
                return ms.ToArray();
            }
        }

        public string DecryptString(string value)
        {
            if (_rsa is null) throw new CryptographicException("Key not set.");
            if (_rsa.PublicOnly) return null;

            var buffer = Convert.FromBase64String(value);
            return buffer.Length < 272 ? "" : Encoding.UTF8.GetString(DecryptBytes(buffer));
        }

        public async Task<string> DecryptStringAsync(string value)
        {
            if (_rsa is null) throw new CryptographicException("Key not set.");
            if (_rsa.PublicOnly) return null;

            var buffer = Convert.FromBase64String(value);
            return buffer.Length < 272 ? "" : Encoding.UTF8.GetString(await DecryptBytesAsync(buffer));
        }

        public void DecryptFile(string srcFile, string dstFile)
        {
            if (_rsa is null) throw new CryptographicException("Key not set.");
            if (_rsa.PublicOnly) return;

            var buffer = DecryptBytes(File.ReadAllBytes(srcFile));
            if (buffer is null) throw new FileNotFoundException("File contents were empty!");
            File.WriteAllBytes(dstFile, buffer);
        }

        public async Task DecryptFileAsync(string srcFile, string dstFile)
        {
            if (_rsa is null) throw new CryptographicException("Key not set.");
            if (_rsa.PublicOnly) return;

            var buffer = await DecryptBytesAsync(File.ReadAllBytes(srcFile));
            if (buffer is null) throw new FileNotFoundException("File contents were empty!");
            File.WriteAllBytes(dstFile, buffer);
        }
    }
}