﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;

#if NETCOREAPP
using System.Text.Json;
#endif

namespace Asfw.IO
{
    public static class Serialization
    {
#if NETCOREAPP
        /// <summary>
        /// Saves a public serializable class object as JSON data in the specified file.
        /// Note: Unlike the XML serializer, all visible variables need to be properties!
        /// </summary>
        public static void SaveJson<T>(string path, T obj)
        {
            var options = new JsonSerializerOptions();
            options.WriteIndented = true;
            var contents = JsonSerializer.Serialize(obj, options);
            File.WriteAllText(path, contents);
        }
    
        /// <summary>
        /// Loads JSON data from the specified file as a public serializable class object.
        /// Note: Unlike the XML serializer, all visible variables need to be properties!
        /// </summary>
        public static T LoadJson<T>(string path)
        {
            var contents = File.ReadAllText(path);
            return (T)JsonSerializer.Deserialize<T>(contents);
        }
#endif

        /// <summary>
        /// Saves a public serializable class object as XML data in the specified file.
        /// </summary>
        public static void SaveXml<T>(string path, T obj)
        {
            using (var streamWriter = new StreamWriter(path))
                new XmlSerializer(typeof(T)).Serialize(streamWriter, obj);
        }

        /// <summary>
        /// Loads XML data from the specified file as a public serializable class object.
        /// </summary>
        public static T LoadXml<T>(string path)
        {
            using (var streamReader = new StreamReader(path))
                return (T)new XmlSerializer(typeof(T)).Deserialize(streamReader);
        }

        /// <summary>
        /// Returns a byte array from a serializable class object.
        /// </summary>
        public static byte[] FromObject(object obj)
        {
            using (var ms = new MemoryStream())
            {
                new BinaryFormatter().Serialize(ms, obj);
                return ms.GetBuffer();
            }
        }

        /// <summary>
        /// Returns an object from a byte array.
        /// </summary>
        public static object ToObject(byte[] bytes)
        {
            using (var ms = new MemoryStream(bytes))
                return new BinaryFormatter().Deserialize(ms);
        }
    }
}