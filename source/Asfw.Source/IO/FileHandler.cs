﻿using System;
using System.IO;
using System.Linq;

namespace Asfw.IO
{
    /// <summary>
    /// Saves and Loads Binary Files using a ByteStream.
    /// </summary>
    public static class ByteFile
    {
        public static void Load(string src, ref ByteStream stream)
        {
            if (!File.Exists(src)) return;

            var br = new BinaryReader(File.Open(src, FileMode.Open));
            stream.Data = br.ReadBytes(br.ReadInt32());
            stream.Head = 0;
            br.Close();
        }

        public static void Save(string dest, ref ByteStream stream)
        {
            var bw = new BinaryWriter(File.Open(dest, FileMode.Create));
            bw.Write(stream.Head);
            bw.Write(stream.ToArray());
            bw.Close();
        }

        public static byte[] Load(string src)
        {
            if (!File.Exists(src)) return new byte[0];

            var br = new BinaryReader(File.Open(src, FileMode.Open));
            var data = br.ReadBytes(br.ReadInt32());
            br.Close();
            return data;
        }

        public static void Save(string dest, ref byte[] data)
        {
            var bw = new BinaryWriter(File.Open(dest, FileMode.Create));
            bw.Write(data.Length);
            bw.Write(data);
            bw.Close();
        }
    }

    public static class TextFile
    {
        /// <summary>
        /// Adds the input on a new line at the end of the text file.
        /// </summary>
        public static void AddLine(string path, string input)
        {
            var contents = File.ReadAllText(path);
            contents += Environment.NewLine + input;
            File.WriteAllText(path, contents);
        }

        /// <summary>
        /// Returns true if the read variable matches the input.
        /// </summary>
        public static bool CompareVar(string path, string header, string name,
            string input)
        {
            return Read(path, header, name) == input;
        }

        /// <summary>
        /// Returns true if the input exists as its own line in the file.
        /// </summary>
        public static bool StringExists(string path, string input)
        {
            var contents = File.ReadAllLines(path);
            return contents.Any(t => t == input);
        }

        /// <summary>
        /// Removes the line matching the input if it exists. The line must match 100%.
        /// </summary>
        public static void RemoveString(string path, string input)
        {
            var contents = File.ReadAllLines(path);
            if (contents.Length < 1) return;

            var fBlock = contents[0];

            if (contents.Length > 1)
                for (var i = 1; i < contents.Length; i++)
                    if (contents[i] != input)
                        fBlock += Environment.NewLine + contents[i];


            File.WriteAllText(path, fBlock);
        }

        /// <summary>
        /// Completely erases the contents of the file.
        /// </summary>
        public static void ClearFile(string path)
        {
            if (!File.Exists(path)) return;
            File.Delete(path);
            File.Create(path).Dispose();
        }

        /// <summary>
        /// Returns the variable data if it is found. Otherwise returns empty string.
        /// </summary>
        public static string Read(string path, string header, string name)
        {
            var contents = File.ReadAllLines(path);
            var i = 0;
            var inHeader = false;

            for (i = 0; i < contents.Length; i++)
            {
                if (inHeader)
                {
                    if (contents[i].StartsWith(name + "=", StringComparison.Ordinal))
                        return contents[i].Substring(name.Length + 1);

                    if (contents[i].StartsWith("[", StringComparison.Ordinal) &&
                        contents[i].EndsWith("]", StringComparison.Ordinal))
                        return "";
                }
                else
                    inHeader |= contents[i].StartsWith("[" + header + "]",
                        StringComparison.Ordinal);
            }

            return "";
        }

        /// <summary>
        /// Overwrites variable data if it is found. Otherwise does nothing.
        /// </summary>
        public static void Write(string path, string header, string name, string value)
        {
            var contents = File.ReadAllLines(path);
            var i = 0;
            var inHeader = false;

            for (i = 0; i < contents.Length; i++)
            {
                if (inHeader)
                {
                    if (!contents[i].StartsWith(name + "=", StringComparison.Ordinal)) continue;

                    contents[i] = name + "=" + value;
                    File.WriteAllLines(path, contents);
                    return;
                }
                else
                    inHeader |= contents[i].StartsWith("[" + header + "]",
                        StringComparison.Ordinal);
            }
        }

        /// <summary>
        /// Returns variable data if found otherwise returns empty string.
        /// This is an INI replica function and creates the variable if it doesnt exist
        /// however this does come with a performance cost.
        /// </summary>
        public static string GetVar(string path, string header, string name)
        {
            var contents = File.ReadAllLines(path);
            var i = 0;
            var inHeader = false;

            for (i = 0; i < contents.Length; i++)
            {
                if (inHeader)
                {
                    if (contents[i].StartsWith(name + "=", StringComparison.Ordinal))
                        return contents[i].Substring(name.Length + 1);

                    if (!contents[i].StartsWith("[", StringComparison.Ordinal) ||
                        !contents[i].EndsWith("]", StringComparison.Ordinal)) continue;

                    var fBlock = contents[0];
                    var cutIndex = i - 1;

                    for (var index = 1; i < contents.Length; index++)
                    {
                        fBlock += Environment.NewLine + contents[i];

                        if (index == cutIndex)
                            fBlock += Environment.NewLine + "[" + header + "]" +
                                      Environment.NewLine + name + "=";
                    }

                    File.WriteAllText(path, fBlock);
                    return "";
                }
                else
                    inHeader |= contents[i].StartsWith("[" + header + "]",
                        StringComparison.Ordinal);
            }

            if (!inHeader)
            {
                var fBlock = "";
                if (contents.Length > 0)
                {
                    fBlock = contents[0];
                    if (contents.Length > 1)
                        for (i = 1; i < contents.Length; i++)
                            fBlock += Environment.NewLine + contents[i];
                }

                fBlock += Environment.NewLine + "[" + header + "]" +
                          Environment.NewLine + name + "=";

                File.WriteAllText(path, fBlock);
            }

            return "";
        }

        /// <summary>
        /// Overwrites variable data if it is found.
        /// This is an INI replica function and creates the variable if it doesnt exist
        /// however this does come with a performance cost.
        /// </summary>
        public static void PutVar(string path, string header, string name, string value)
        {
            var contents = File.ReadAllLines(path);
            var i = 0;
            var inHeader = false;

            for (i = 0; i < contents.Length; i++)
            {
                if (inHeader)
                {
                    if (contents[i].StartsWith(name + "=", StringComparison.Ordinal))
                    {
                        contents[i] = name + "=" + value;
                        File.WriteAllLines(path, contents);
                        return;
                    }

                    if (!contents[i].StartsWith("[", StringComparison.Ordinal) ||
                        !contents[i].EndsWith("]", StringComparison.Ordinal)) continue;

                    var fBlock = contents[0];
                    var cutIndex = i - 1;

                    for (var index = 1; i < contents.Length; index++)
                    {
                        fBlock += Environment.NewLine + contents[i];

                        if (index == cutIndex)
                            fBlock += Environment.NewLine + "[" + header + "]" +
                                      Environment.NewLine + name + "=" + value;
                    }

                    File.WriteAllText(path, fBlock);
                    return;
                }
                else
                    inHeader |= contents[i].StartsWith("[" + header + "]",
                        StringComparison.Ordinal);
            }

            if (!inHeader)
            {
                var fBlock = "";
                if (contents.Length > 0)
                {
                    fBlock = contents[0];
                    if (contents.Length > 1)
                        for (i = 1; i < contents.Length; i++)
                            fBlock += Environment.NewLine + contents[i];
                }

                fBlock += Environment.NewLine + "[" + header + "]" +
                          Environment.NewLine + name + "=" + value;

                File.WriteAllText(path, fBlock);
            }
        }
    }
}